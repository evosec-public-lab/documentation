Fluxo de Autenticação
======================

O fluxo de autenticação implementado pelo uAuthenticator pode ser definido por etapas. Foi pensado para desvincular a identidade do usuário da sessão do usuário.

==================================
1. Autenticação da Conta de Acesso
==================================

A autenticação é o processo de identificar a conta de acesso através da combinação de identificador e senha. Nessa etapa a aplicação deverá consumir o método ``uAuthenticator.Access.signIn``. Em caso de sucesso o método te devolverá um JWT que é conhecido como **id_token**, você poderá decodificar esse token para capturar informações importantes como o campo ``multi_factor_method``. Esse campo informa o método que será utilizado para trocar o **id_token** pelos tokens: **access_token** e **refresh_token**.

=====================
2. Câmbio do id_token
=====================

O câmbio do **id_token** é o processo de utilizar um token de autenticação para obter um token de acesso. Para isso será preciso identificar qual o método de câmbio será utilizado. Essa informação pode ser obtida através da decodificação do **id_token** capturando o valor do atributo ``multi_factor_method``.

As correspondências dos valores do ``multi_factor_method`` para os métodos de câmbio são:

* ``none`` indica que a conta de acesso não utiliza um segundo fator de autenticação, o método que deverá ser consumido para câmbio nesse caso é: ``uAuthenticator.Access.exchange``.
* ``time_token`` indica que a conta de acesso utiliza um TOTP (Time-based One-time Password) para obter o acesso, o método que deverá ser consumido para câmbio nesse caso é: ``uAuthenticator.Access.exchangeTimeToken``.

=========================
3. Autenticação do Acesso
=========================

Após o câmbio você obterá dois tokens: **access_token** e **refresh_token**. O **access_token** é utilizado para realizar a autenticação do acesso, você deverá utilizar o método ``uAuthenticator.Access.authenticate`` para tanto. Partindo desse momento sua conexão está autenticada. A sua sessão estará autenticada pelo mesmo período em que o **access_token** está válido. Já o **refresh_token** é utilizado para gerar novos tokens de acesso para manter sua sessão autenticada, isso será visto no próximo passo.

======================
4. Renovação de Acesso
======================

Normalmente o **access_token** expira em um tempo breve. Isso permite que cada sessão seja volátil em questões de segurança, ou seja, é possível invalidar um **refresh_token** assim o acesso não autorizado não poderá ser renovado em um breve período de tempo finalizando a autenticação da sessão. Porém para manter sua sessão autenticada você deverá utilizar o seu **refresh_token** com o método ``uAuthenticator.acess.refresh``, esse método irá retornar um novo **access_token** que deverá ser utilizado no método ``uAuthenticator.Access.authenticate``.

========================
5. Finalização de Acesso
========================

Para finalizar a autenticação da sua sessão basta consumir o método ``uAuthenticator.Access.finish``. Esse método irá invalidar a autenticação da sessão.