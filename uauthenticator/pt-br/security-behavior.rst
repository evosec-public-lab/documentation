SecurityBehavior
================

Os comportamentos de segurança definem parâmetros para o fluxo de geração de tokens do uAuthenticator. Abaixo são descritos os métodos disponíveis do serviço:

--------------------------------------
uAuthenticator.SecurityBehavior.create
--------------------------------------

Cria um comportamento de segurança. Alguns parâmetros são importantes e estão descritos na tabela abaixo:

+-----------------------------+---------+--------------------------------------------------------------------------------------------+
| Campo                       | Tipo    | Descrição                                                                                  |
+=============================+=========+============================================================================================+
| require_multi_factor_method | string  | Esse campo define qual a política de 2FA para a geração de tokens de acesso.               |
|                             |         | Os valores possíveis para esse campo são:                                                  |
|                             |         |                                                                                            |
|                             |         | * ``null`` define que não utiliza 2FA.                                                     |
|                             |         | * ``time_token`` define que utiliza TOTP para gerar tokens de acesso.                      |
+-----------------------------+---------+--------------------------------------------------------------------------------------------+
| require_multi_factor        | boolean | Caso ``true`` define que o token de identificação gerado (**id_token**) exija              |
|                             |         | o **require_multi_factor_method** definido como único possível para realizar               |
|                             |         | a geração de tokens de acesso.                                                             |
+-----------------------------+---------+--------------------------------------------------------------------------------------------+
| maximum_exchange_attempts   | integer | Número máximo de tentativas permitidas de câmbio de um **id_token** por                    |
|                             |         | tokens de acesso. O valor mínimo permitido é ``1``. Com esse valor, caso **id_token**      |
|                             |         | utilizado para câmbio seja utilizado de forma incorreta (TOTP incorreto por exemplo)       |
|                             |         | não será permitida uma tentativa com o mesmo token devendo gerar novamente um **id_token** |
|                             |         | para realizar o câmbio.                                                                    |
+-----------------------------+---------+--------------------------------------------------------------------------------------------+

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"require_multi_factor_method": "time_token",
		"require_multi_factor": true,
		"maximum_exchange_attempts": 3
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"security_behavior_uuid": "e4a453f3-6953-45c3-b5ab-511edae1b778",
		"require_multi_factor": true,
		"require_multi_factor_method": "time_token",
		"maximum_exchange_attempts": 3
	}

--------------------------------------
uAuthenticator.SecurityBehavior.update
--------------------------------------

Altera dados de um comportamento de segurança. Utiliza os mesmos parâmetros do método ``uAuthenticator.SecurityBehavior.create``.
É obrigatório fornecer o ``security_behavior_uuid`` sendo o UUIDv4 do comportamento de segurança que deseja atualizar.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"security_behavior_uuid": "e4a453f3-6953-45c3-b5ab-511edae1b778",
		"require_multi_factor_method": "time_token",
		"require_multi_factor": false,
		"maximum_exchange_attempts": 1
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"security_behavior_uuid": "e4a453f3-6953-45c3-b5ab-511edae1b778",
		"require_multi_factor": false,
		"require_multi_factor_method": "time_token",
		"maximum_exchange_attempts": 1
	}

-----------------------------------
uAuthenticator.SecurityBehavior.get
-----------------------------------

Retorna os dados de um comportamento de segurança.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"security_behavior_uuid": "e4a453f3-6953-45c3-b5ab-511edae1b778"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"security_behavior_uuid": "e4a453f3-6953-45c3-b5ab-511edae1b778",
		"require_multi_factor": false,
		"require_multi_factor_method": "time_token",
		"maximum_exchange_attempts": 1
	}

--------------------------------------
uAuthenticator.SecurityBehavior.search
--------------------------------------

Busca um comportamento de segurança. Para entender o valor do campo **search_query** visite a documentação da estrutura de busca,
dos microserviços.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"search_query": "page=1&eq[require_multi_factor_method]=time_token"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"total_count": 4,
		"results": [
			{
				"security_behavior_uuid": "c5118835-6090-468f-ac51-6b95a27f853e",
				"require_multi_factor": true,
				"require_multi_factor_method": "time_token",
				"maximum_exchange_attempts": 3
			},
			{
				"security_behavior_uuid": "72945db1-7566-412e-9026-eaf3fec662fe",
				"require_multi_factor": true,
				"require_multi_factor_method": "time_token",
				"maximum_exchange_attempts": 5
			},
			{
				"security_behavior_uuid": "60cd8953-91fc-43bc-9fab-a43fb35a32c5",
				"require_multi_factor": false,
				"require_multi_factor_method": "time_token",
				"maximum_exchange_attempts": 1
			},
			{
				"security_behavior_uuid": "e4a453f3-6953-45c3-b5ab-511edae1b778",
				"require_multi_factor": false,
				"require_multi_factor_method": "time_token",
				"maximum_exchange_attempts": 1
			}
		]
	}

--------------------------------------
uAuthenticator.SecurityBehavior.delete
--------------------------------------

Deleta um comportamento de segurança. **Atenção!** Essa ação não tem retorno.


^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"security_behavior_uuid": "e4a453f3-6953-45c3-b5ab-511edae1b778"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	"e4a453f3-6953-45c3-b5ab-511edae1b778"