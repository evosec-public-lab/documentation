Federation
==========

As federações são divisões lógicas das políticas e controle de autenticação do uAuthenticator. Abaixo são descritos os métodos disponíveis do serviço:

--------------------------------
uAuthenticator.Federation.create
--------------------------------

Cria uma federação. Alguns parâmetros são importantes e estão descritos na tabela abaixo:

+-------------------------------------+---------------+--------------------------------------------------------------------------------------+
| Campo                               | Tipo          | Descrição                                                                            |
+=====================================+===============+======================================================================================+
| security_configuration_uuid         | UUIDv4        | Configuração de segurança que será aplicada na federação.                            |
+-------------------------------------+---------------+--------------------------------------------------------------------------------------+
| security_behavior_uuid              | UUIDv4        | Comportamento de segurança utilizado pela federação,                                 |
|                                     |               | caso o campo **allow_account_own_security_behavior** tenha                           |
|                                     |               | o valor ``true`` cada conta de acesso poderá ter seu comportamento                   |
|                                     |               | de segurança, utilizando esse valor como padrão.                                     |
+-------------------------------------+---------------+--------------------------------------------------------------------------------------+
| name                                | i18n          | Campo internacionalizável, um objeto com as chaves sendo os códigos                  |
|                                     |               | das linguagens e valores sendo as strings que representam o nome da federação,       |
|                                     |               | para uma lista das linguagens suportadas consulte os responsáveis pelo microserviço. |
+-------------------------------------+---------------+--------------------------------------------------------------------------------------+
| secret                              | string        | Chave secreta que será utilizada para criptografar dados da federação, é importante  |
|                                     |               | não armazenar essa chave em nenhum outro lugar, sendo apenas conhecida pelos         |
|                                     |               | responsáveis pela federação.                                                         |
+-------------------------------------+---------------+--------------------------------------------------------------------------------------+
| public_rsa_key, private_rsa_key     | string        | Chaves utilizadas para gerar assinaturas pela federação, caso não sejam fornecidas,  |
|                                     |               | o uAuthenticator irá gerar as chaves automaticamente.                                |
+-------------------------------------+---------------+--------------------------------------------------------------------------------------+
| allow_account_own_security_behavior | boolean       | Indica se a federação permite que as contas de acesso tenham seu próprio             |
|                                     |               | **security_behavior_uuid**. Caso ``false`` todas as contas de acesso utilizarão      |
|                                     |               | o comportamento de segurança definido na federação.                                  |
+-------------------------------------+---------------+--------------------------------------------------------------------------------------+
| attributes                          | array<object> | Um array de objetos contendo valores customizados de atributos da federação.         |
|                                     |               | Esse campo pode ser utilizado para definir parâmetros customizados para front-end ou |
|                                     |               | outros sistemas/aplicações. Esse campo pode ser um array vazio.                      |
+-------------------------------------+---------------+--------------------------------------------------------------------------------------+

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"security_configuration_uuid": "4d6142a7-7ffe-4613-9dd1-8643bb0fd836",
		"security_behavior_uuid": "13d6d169-d2cd-48cf-9d28-3ea4cc98ea93",
		"name":{
			"eng": "Federation",
			"por": "Federação"
		},
		"secret": "meu_segredo",
		"public_rsa_key": null,
		"private_rsa_key": null,
		"allow_account_own_security_behavior": false,
		"attributes": [
			{
				"name": "atributo_customizado",
				"value": "valor_do_atributo"
			}
		]
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"federation_uuid": "783a9a34-8c16-4dfd-90f3-7f3e1e4be901",
		"security_configuration_uuid": "4d6142a7-7ffe-4613-9dd1-8643bb0fd836",
		"security_behavior_uuid": "13d6d169-d2cd-48cf-9d28-3ea4cc98ea93",
		"name": {
			"eng": "Federation",
			"por": "Federação"
		},
		"secret": null,
		"public_rsa_key": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoycxgoHLBOELecmGeusj\ny0CfPzSJ9C3qy8r0e/P+JnbNUHjq7ri1mt4BQ5ZV2/D8vGh//L1iilcWZGsAG76H\nM/U50RLGh4HzkUkj4KVs1QOhLVwmYqfWMIq+HCM8Eovv1q4c3EEir/kY9YomAK2b\nkpvDm3ezkuRnocUw+Pn4L954lmdxycl4ywqsmqZOHwfY6pQ6Xf+hbZUUDJOLLx63\n/nVXs1bcwuAQuRbG2rBqCMMuH46n72B7oKW36H+oPbniuQExDFlz9u99mpQFaA1o\n6famPmDGlB3T8ssJ4g7+D8kKXHLGov+Cg5G/+4n2KAvMtOfWRKq5T5lB8XS/13iv\nvwIDAQAB\n-----END PUBLIC KEY-----\n",
		"private_rsa_key": null,
		"allow_account_own_security_behavior": false,
		"attributes": [
			{
				"name": "atributo_customizado",
				"value": "valor_do_atributo"
			}
		]
	}

--------------------------------
uAuthenticator.Federation.update
--------------------------------

Altera dados de uma federação. Utiliza os mesmos parâmetros do método ``uAuthenticator.Federation.create``.
É obrigatório fornecer o ``federation_uuid`` sendo o UUIDv4 da federação que deseja atualizar.

**Notas:**

* Caso o campo **secret** tenha o valor ``null`` o valor não é alterado na federação.
* A mesma política se aplica para os campos **public_rsa_key** e **private_rsa_key**.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"federation_uuid": "783a9a34-8c16-4dfd-90f3-7f3e1e4be901",
		"security_configuration_uuid": "4d6142a7-7ffe-4613-9dd1-8643bb0fd836",
		"security_behavior_uuid": "13d6d169-d2cd-48cf-9d28-3ea4cc98ea93",
		"name": {
			"eng": "Federation",
			"por": "Federação"
		},
		"secret": null,
		"public_rsa_key": null,
		"private_rsa_key": null,
		"allow_account_own_security_behavior": false,
		"attributes": [
			{
				"name": "atributo_customizado",
				"value": "valor_do_atributo"
			}
		]
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"federation_uuid": "783a9a34-8c16-4dfd-90f3-7f3e1e4be901",
		"security_configuration_uuid": "4d6142a7-7ffe-4613-9dd1-8643bb0fd836",
		"security_behavior_uuid": "13d6d169-d2cd-48cf-9d28-3ea4cc98ea93",
		"name": {
			"eng": "Federation",
			"por": "Federação"
		},
		"secret": null,
		"public_rsa_key": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoycxgoHLBOELecmGeusj\ny0CfPzSJ9C3qy8r0e/P+JnbNUHjq7ri1mt4BQ5ZV2/D8vGh//L1iilcWZGsAG76H\nM/U50RLGh4HzkUkj4KVs1QOhLVwmYqfWMIq+HCM8Eovv1q4c3EEir/kY9YomAK2b\nkpvDm3ezkuRnocUw+Pn4L954lmdxycl4ywqsmqZOHwfY6pQ6Xf+hbZUUDJOLLx63\n/nVXs1bcwuAQuRbG2rBqCMMuH46n72B7oKW36H+oPbniuQExDFlz9u99mpQFaA1o\n6famPmDGlB3T8ssJ4g7+D8kKXHLGov+Cg5G/+4n2KAvMtOfWRKq5T5lB8XS/13iv\nvwIDAQAB\n-----END PUBLIC KEY-----\n",
		"private_rsa_key": null,
		"allow_account_own_security_behavior": false,
		"attributes": [
			{
				"name": "atributo_customizado",
				"value": "valor_do_atributo"
			}
		]
	}

--------------------------------
uAuthenticator.Federation.get
--------------------------------

Retorna os dados de uma federação.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"federation_uuid": "783a9a34-8c16-4dfd-90f3-7f3e1e4be901"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"federation_uuid": "783a9a34-8c16-4dfd-90f3-7f3e1e4be901",
		"security_configuration_uuid": "4d6142a7-7ffe-4613-9dd1-8643bb0fd836",
		"security_behavior_uuid": "13d6d169-d2cd-48cf-9d28-3ea4cc98ea93",
		"name": {
			"eng": "Federation",
			"por": "Federação"
		},
		"secret": null,
		"public_rsa_key": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoycxgoHLBOELecmGeusj\ny0CfPzSJ9C3qy8r0e/P+JnbNUHjq7ri1mt4BQ5ZV2/D8vGh//L1iilcWZGsAG76H\nM/U50RLGh4HzkUkj4KVs1QOhLVwmYqfWMIq+HCM8Eovv1q4c3EEir/kY9YomAK2b\nkpvDm3ezkuRnocUw+Pn4L954lmdxycl4ywqsmqZOHwfY6pQ6Xf+hbZUUDJOLLx63\n/nVXs1bcwuAQuRbG2rBqCMMuH46n72B7oKW36H+oPbniuQExDFlz9u99mpQFaA1o\n6famPmDGlB3T8ssJ4g7+D8kKXHLGov+Cg5G/+4n2KAvMtOfWRKq5T5lB8XS/13iv\nvwIDAQAB\n-----END PUBLIC KEY-----\n",
		"private_rsa_key": null,
		"allow_account_own_security_behavior": false,
		"attributes": [
			{
				"name": "atributo_customizado",
				"value": "valor_do_atributo"
			}
		]
	}

--------------------------------
uAuthenticator.Federation.search
--------------------------------

Busca uma federação. Para entender o valor do campo **search_query** visite a documentação da estrutura de busca,
dos microserviços.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"search_query": "page=1&language=por&eq[i18n.name]=Federação"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"total_count": 1,
		"results": [
			{
				"federation_uuid": "783a9a34-8c16-4dfd-90f3-7f3e1e4be901",
				"security_configuration_uuid": "4d6142a7-7ffe-4613-9dd1-8643bb0fd836",
				"security_behavior_uuid": "13d6d169-d2cd-48cf-9d28-3ea4cc98ea93",
				"name": {
					"por": "Federação"
				},
				"secret": null,
				"public_rsa_key": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoycxgoHLBOELecmGeusj\ny0CfPzSJ9C3qy8r0e/P+JnbNUHjq7ri1mt4BQ5ZV2/D8vGh//L1iilcWZGsAG76H\nM/U50RLGh4HzkUkj4KVs1QOhLVwmYqfWMIq+HCM8Eovv1q4c3EEir/kY9YomAK2b\nkpvDm3ezkuRnocUw+Pn4L954lmdxycl4ywqsmqZOHwfY6pQ6Xf+hbZUUDJOLLx63\n/nVXs1bcwuAQuRbG2rBqCMMuH46n72B7oKW36H+oPbniuQExDFlz9u99mpQFaA1o\n6famPmDGlB3T8ssJ4g7+D8kKXHLGov+Cg5G/+4n2KAvMtOfWRKq5T5lB8XS/13iv\nvwIDAQAB\n-----END PUBLIC KEY-----\n",
				"private_rsa_key": null,
				"allow_account_own_security_behavior": false,
				"attributes": [
					{
						"name": "atributo_customizado",
						"value": "valor_do_atributo"
					}
				]
			}
		]
	}

--------------------------------
uAuthenticator.Federation.delete
--------------------------------

Deleta uma federação. **Atenção!** Essa ação não tem retorno.


^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"federation_uuid": "783a9a34-8c16-4dfd-90f3-7f3e1e4be901"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	"783a9a34-8c16-4dfd-90f3-7f3e1e4be901"
