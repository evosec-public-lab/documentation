Documentação do uAuthenticator
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Conteúdo:

   Estrutura dos serviços <estrutura-servicos>
   Fluxo de autenticação <fluxo-autenticacao>
   Federation <federation>
   SecurityBehavior <security-behavior>
   SecurityConfiguration <security-configuration>
   AccessAccount <access-account>
   AccountGroup <account-group>
   Access <access>
   IdentityProvider <identity-provider>