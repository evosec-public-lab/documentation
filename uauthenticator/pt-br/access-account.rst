AccessAccount
=============

As contas de acesso são a unidade de autenticação mais granular do uAuthenticator. Elas podem representar um usuário ou uma conta de serviço (utilizada por integrações inter-sistêmicas). Abaixo são descritos os métodos disponíveis do serviço:

-----------------------------------
uAuthenticator.AccessAccount.create
-----------------------------------

Cria uma conta de acesso. Alguns parâmetros são importantes e estão descritos na tabela abaixo:

+------------------------+---------------+-----------------------------------------------------------------------------------------------------------------+
| Campo                  | Tipo          | Descrição                                                                                                       |
+========================+===============+=================================================================================================================+
| security_behavior_uuid | string        | Comportamento de segurança da conta de acesso, lembrando que esse parâmetro só tera efeito                      |
|                        |               | caso esteja permitido pela federação.                                                                           |
+------------------------+---------------+-----------------------------------------------------------------------------------------------------------------+
| is_service_account     | boolean       | Indica se é uma conta de serviço. Contas de serviço podem ter permissões especiais no ambiente                  |
|                        |               | dos microserviços.                                                                                              |
+------------------------+---------------+-----------------------------------------------------------------------------------------------------------------+
| is_master              | boolean       | Define se é uma conta "master". A conta master tem privilégios de administração do sistema. Através             |
|                        |               | da API não será possível criar uma conta master, portando mesmo que seja fornecido ``true`` nesse campo         |
|                        |               | o uAuthenticator irá forçar esse valor para ``false``.                                                          |
+------------------------+---------------+-----------------------------------------------------------------------------------------------------------------+
| is_blocked             | boolean       | Indica se a conta de acesso está bloqueada. Isso desabilita a possibilidade realizar a geração de **id_token**. |
+------------------------+---------------+-----------------------------------------------------------------------------------------------------------------+
| attributes             | array<object> | Um array de objetos contendo valores customizados de atributos da conta de acesso.                              |
|                        |               | Esse campo pode ser utilizado para definir parâmetros customizados para front-end ou                            |
|                        |               | outros sistemas/aplicações. Esse campo pode ser um array vazio.                                                 |
+------------------------+---------------+-----------------------------------------------------------------------------------------------------------------+
| identifiers            | array<string> | Um array de strings que serão identificadores da conta de acesso. Esses valores devem ser únicos ou seja:       |
|                        |               | não podem existir duas contas de acesso com os mesmos identificadores. Identificadores são análogos aos "nomes  |
|                        |               | de usuário" em outros sistemas.                                                                                 |
+------------------------+---------------+-----------------------------------------------------------------------------------------------------------------+

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"security_behavior_uuid": "13d6d169-d2cd-48cf-9d28-3ea4cc98ea93",
		"name": "Evosec",
		"password": "qwerY@12345",
		"email": "security@evosec.io",
		"is_service_account": false,
		"is_blocked": false,
		"attributes": [
				{
						"name": "meu_atributo",
						"value": "meu_valor"
				}
		],
		"identifiers": [
			"evosec_security",
			"security@evosec.io"
		]
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"access_account_uuid": "a543f876-958d-4636-9fac-c8176c489e57",
		"security_behavior_uuid": "13d6d169-d2cd-48cf-9d28-3ea4cc98ea93",
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"name": "Evosec",
		"identifiers": [
			"evosec_security",
			"security@evosec.io"
		],
		"is_service_account": false,
		"is_master": false,
		"email": "security@evosec.io",
		"password": null,
		"password_expiration_date": "2018-10-07",
		"totp_secret_key": null,
		"attributes": [
			{
				"name": "meu_atributo",
				"value": "meu_valor"
			}
		],
		"is_blocked": false
	}

-----------------------------------
uAuthenticator.AccessAccount.update
-----------------------------------

Altera dados de uma conta de acesso. Utiliza os mesmos parâmetros do método ``uAuthenticator.AccessAccount.create``.
É obrigatório fornecer o ``access_account_uuid`` sendo o UUIDv4 da conta de acesso que deseja atualizar.

**Notas:**

* Caso o valor de **password** seja ``null`` a senha não é alterada.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"access_account_uuid": "a543f876-958d-4636-9fac-c8176c489e57",
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"security_behavior_uuid": "13d6d169-d2cd-48cf-9d28-3ea4cc98ea93",
		"name": "Evosec Security",
		"password": null,
		"email": "security@evosec.io",
		"is_service_account": false,
		"is_blocked": false,
		"attributes": [
				{
						"name": "meu_atributo",
						"value": "meu_valor"
				}    
		],
		"identifiers": [
			"evosec_security",
			"security@evosec.io"
		]
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"access_account_uuid": "a543f876-958d-4636-9fac-c8176c489e57",
		"security_behavior_uuid": "13d6d169-d2cd-48cf-9d28-3ea4cc98ea93",
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"name": "Evosec Security",
		"identifiers": [
			"evosec_security",
			"security@evosec.io"
		],
		"is_service_account": false,
		"is_master": false,
		"email": "security@evosec.io",
		"password": null,
		"password_expiration_date": "2018-10-07",
		"totp_secret_key": null,
		"attributes": [
			{
				"name": "meu_atributo",
				"value": "meu_valor"
			}
		],
		"is_blocked": false
	}

--------------------------------
uAuthenticator.AccessAccount.get
--------------------------------

Retorna os dados de uma conta de acesso.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"access_account_uuid": "a543f876-958d-4636-9fac-c8176c489e57"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"access_account_uuid": "a543f876-958d-4636-9fac-c8176c489e57",
		"security_behavior_uuid": "13d6d169-d2cd-48cf-9d28-3ea4cc98ea93",
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"name": "Evosec Security",
		"identifiers": [
			"evosec_security",
			"security@evosec.io"
		],
		"is_service_account": false,
		"is_master": false,
		"email": "security@evosec.io",
		"password": null,
		"password_expiration_date": "2018-10-07",
		"totp_secret_key": null,
		"attributes": [
			{
				"name": "meu_atributo",
				"value": "meu_valor"
			}
		],
		"is_blocked": false
	}

-----------------------------------
uAuthenticator.AccessAccount.search
-----------------------------------

Busca uma conta de acesso. Para entender o valor do campo **search_query** visite a documentação da estrutura de busca, dos microserviços.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"search_query": "page=1&wrd[name]=Evosec"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"total_count": 1,
		"results": [
			{
				"access_account_uuid": "a543f876-958d-4636-9fac-c8176c489e57",
				"security_behavior_uuid": "13d6d169-d2cd-48cf-9d28-3ea4cc98ea93",
				"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
				"name": "Evosec Security",
				"identifiers": [
					"evosec_security",
					"security@evosec.io"
				],
				"is_service_account": false,
				"is_master": false,
				"email": "security@evosec.io",
				"password": null,
				"password_expiration_date": "2018-10-07",
				"totp_secret_key": null,
				"attributes": [
					{
						"name": "meu_atributo",
						"value": "meu_valor"
					}
				],
				"is_blocked": false
			}
		]
	}

-----------------------------------
uAuthenticator.AccessAccount.delete
-----------------------------------

Deleta uma conta de acesso. **Atenção!** Essa ação não tem retorno.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"access_account_uuid": "a543f876-958d-4636-9fac-c8176c489e57"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	"a543f876-958d-4636-9fac-c8176c489e57"

----------------------------------------------------
uAuthenticator.AccessAccount.generateTimeTokenSecret
----------------------------------------------------

Gera um secret para TOTP da conta de acesso. O valor do campo **qr_code** na resposta pode ser usado diretamente na tag HTML ``<img src="[qr_code]">``.


^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"access_account_uuid": "a543f876-958d-4636-9fac-c8176c489e57"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"time_token_secret": "6d69275735bc74f61661b05b67be4fef",
		"qr_code": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMQAAADECAYAAADApo5rAAAAAklEQVR4AewaftIAAAjHSURBVO3BQYolyZIAQdUg739lneIvHFs5BO9ldU9jIvYHa63/eVhrHQ9rreNhrXU8rLWOh7XW8bDWOh7WWsfDWut4WGsdD2ut42GtdTystY6HtdbxsNY6HtZaxw8fUvmbKv5JKjcVb6hMFTcqNxWTylQxqbxRMalMFZPK31TxiYe11vGw1joe1lrHD19W8U0qn1CZKm5UbireULmpuFH5RMW/ScU3qXzTw1rreFhrHQ9rreOHX6byRsUbKjcVNypvqEwVNxWTyqQyVUwVn1CZKqaKSWWq+E0qb1T8poe11vGw1joe1lrHD/9xKv8klaliUplUPlExVUwqn1D5L3tYax0Pa63jYa11/PAfUzGp3FT8popJ5Y2KSeUNlaliUvlExX/Jw1rreFhrHQ9rreOHX1bxT6p4Q2WqmFRuKm4qJpUblanijYpJ5RMVk8pU8UbFv8nDWut4WGsdD2ut44cvU/k3UZkqJpWpYlKZKiaVG5Wp4qZiUrlRmSomlaliUpkqJpWp4hMq/2YPa63jYa11PKy1jh8+VPFvonKjMlVMKjcqn1CZKiaVb6q4qbip+ETF/ycPa63jYa11PKy1DvuDD6hMFZPKN1W8oTJVTCpTxRsqb1S8ofKJiknlExVvqHxTxW96WGsdD2ut42Gtdfzwl1XcqEwVk8obFW+oTBWTyk3FpDKp3FRMFZPKVPFGxaQyVUwqk8pUMancVEwq/yYPa63jYa11PKy1DvuDD6hMFTcqn6iYVH5TxY3KGxWTyk3FjcpUMancVEwqNxU3Km9UTCqfqPjEw1rreFhrHQ9rrcP+4BepTBU3KlPFpHJTcaPyiYo3VKaKSeWNijdU3qiYVG4q3lCZKiaVqWJSuan4xMNa63hYax0Pa63jh3+ZiknlmypuVKaKSeWNipuKb1J5o2JS+YTKN6lMFZPKNz2stY6HtdbxsNY6fvgylW+qeENlqvhNFTcqU8WkMlVMKlPFpDJVTCo3KlPFGypTxY3KpDJVTCqTym96WGsdD2ut42Gtdfzwl6lMFZPKTcUbKjcVU8Wk8obKjcpU8YmKm4pPqNxUfKLipmJSmSq+6WGtdTystY6Htdbxw5dV3KjcVLyhMlVMKjcqNxWTyhsVk8qk8gmVNyomlX8TlaliqvhND2ut42GtdTystY4fPqQyVXyTylQxVdxUTCpTxaRyU/FNFd9U8U0Vf1PFGypTxSce1lrHw1rreFhrHT/8ZRWTylQxVUwqNxWfqLhRmSo+oXJTMancqNxUTBWTyqRyU3GjcqMyVdyo/KaHtdbxsNY6HtZah/3BL1K5qbhReaPiDZU3Km5UpooblZuKN1TeqHhD5Y2KSeWNikllqvimh7XW8bDWOh7WWof9wRepTBVvqEwVNyqfqLhR+aaKSeWNihuVqeINlaniEypTxaQyVUwqNxXf9LDWOh7WWsfDWuv44UMqNyqfULmpuFG5UZkq3qh4Q2WqmFSmikllqpgqJpWbihuVm4qbiknl3+xhrXU8rLWOh7XW8cOHKt5QmSq+SeVvUpkqJpUblRuVN1SmihuVb1L5m1Smik88rLWOh7XW8bDWOn74ZSo3KlPFpPKbKj5RcVNxozJVTCpTxaRyo/KJihuVqWJS+aaK3/Sw1joe1lrHw1rr+OHLVKaKSWWqmFSmikllqphUpopJZVJ5o2JSmSo+oTJV3FTcqLxRcaMyVXyiYlKZKiaVm4pPPKy1joe11vGw1jrsDz6gMlW8oTJVTCpTxY3KTcUnVKaKSeWm4g2VqWJS+U0VNypTxaTyRsWkMlX8poe11vGw1joe1lrHD3+ZylQxqUwVNyo3FZPKTcWkMlXcVNyo3FRMFZPKVHGjMlVMKlPFjcpUcVPxhspUMancVHziYa11PKy1joe11mF/8EUqNxWTyhsVk8pUMancVLyhMlX8JpWpYlK5qZhUbiomlaniDZWbik+oTBWfeFhrHQ9rreNhrXXYH3xA5RMVk8pUMalMFW+o/KaKG5VvqviEylQxqUwVNyo3FZPKTcWkMlV808Na63hYax0Pa63D/uADKm9U3KjcVLyhMlXcqHxTxaRyUzGpfFPFJ1RuKiaVqeJG5Y2Kb3pYax0Pa63jYa112B98QOU3VUwqf1PFjcpU8U0qU8WNylQxqUwVk8pUcaPymypuVKaKTzystY6HtdbxsNY6fviyim9S+aaKv0llqphU3lD5J6lMFd+kcqMyVXzTw1rreFhrHQ9rreOHX6byRsVU8YbKjcobFTcVk8obFZPKVDGpTBWTyqTyRsUbKjcVk8q/2cNa63hYax0Pa63D/uADKlPFGypvVHxCZar4hMpU8YbKVHGjMlW8oXJTMalMFZPK31Txmx7WWsfDWut4WGsd9gf/j6ncVLyh8omKSeWNihuVm4pJZar4hMpNxRsqU8UbKlPFJx7WWsfDWut4WGsdP3xI5W+qmComlUnlpuKfVHGjMlW8UXGjMlV8k8pU8YbKTcU3Pay1joe11vGw1jrsDz6gMlV8k8pUMal8U8WkMlV8QuWm4g2VqWJSmSreUJkqJpWp4g2Vb6r4xMNa63hYax0Pa63jh1+m8kbFJypuVG5UvknlpmJSuamYKm4qJpWpYlKZKiaVG5W/qeKbHtZax8Na63hYax0//Mep3FTcqNyoTBVTxaQyqUwVNypTxY3KVDGpTBWTyk3FN6ncVEwqU8UnHtZax8Na63hYax0//MdVTCpvVNxUTCo3FZPKpDJVTBWfUHmj4kblb1KZKr7pYa11PKy1joe11vHDL6v4TRU3KlPFjcpUMalMFTcVk8pUMalMKlPFpDJV3FS8oTJV3FS8oTJVTCp/08Na63hYax0Pa63jhy9T+ZtUbiomlaniRuWbKm4qJpVJ5UblRmWqmFSmikllqvibKn7Tw1rreFhrHQ9rrcP+YK31Pw9rreNhrXU8rLWOh7XW8bDWOh7WWsfDWut4WGsdD2ut42GtdTystY6HtdbxsNY6HtZax8Na6/g/CTmrrMClIWYAAAAASUVORK5CYII=",
		"uri": "otpauth://totp/undefined%20(Evosec%20Security)?secret=GZSDMOJSG42TOMZVMJRTONDGGYYTMNRRMIYDKYRWG5RGKNDGMVTA"
	}

---------------------------------------------
uAuthenticator.AccessAccount.getAccountGroups
---------------------------------------------

Retorna a hierarquia de grupos de contas de uma conta de acesso.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"access_account_uuid": "a543f876-958d-4636-9fac-c8176c489e57"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"superordinates": [
			{
				"account_group_uuid": "5e0ad612-8f57-4930-832f-00f4c819920b"
			}
		],
		"subordinates": [
			{
				"account_group_uuid": "f243550f-573f-452d-8ce9-1f17c87ed9c4"
			}
		],
		"associated": [
			{
				"account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c"
			}
		]
	}