SecurityConfiguration
=====================

As configurações de segurança definem parâmetros para garantir um ambiente seguro de autenticação implementando por federação políticas para tal fluxo. Abaixo são descritos os métodos disponíveis do serviço:

-------------------------------------------
uAuthenticator.SecurityConfiguration.create
-------------------------------------------

Cria uma configuração de segurança. Alguns parâmetros são importantes e estão descritos na tabela abaixo:

+----------------------------------+---------+---------------------------------------------------------------------------------------------------------+
| Campo                            | Tipo    | Descrição                                                                                               |
+==================================+=========+=========================================================================================================+
| default_password_expiration_days | integer | Tempo em dias para expiração de senha das contas de acesso. Defina ``0`` para que a senha nunca expire. |
+----------------------------------+---------+---------------------------------------------------------------------------------------------------------+
| short_term_expiration_seconds    | integer | Tempo em segundos para expiração do **access_token** e **id_token**.                                    |
+----------------------------------+---------+---------------------------------------------------------------------------------------------------------+
| long_term_expiration_seconds     | integer | Tempo em segundos para expiração do **refresh_token**.                                                  |
+----------------------------------+---------+---------------------------------------------------------------------------------------------------------+
| minimum_password_length          | integer | Tamanho mínimo permitido para senhas das contas de acesso. O valor mínimo para esse campo é ``3``.      |
+----------------------------------+---------+---------------------------------------------------------------------------------------------------------+
| password_security_level          | integer | Nível de segurança das senhas das contas de acesso:                                                     |
|                                  |         |                                                                                                         |
|                                  |         | * ``0`` sem política de segurança.                                                                      |
|                                  |         | * ``1`` exige que a senha tenha números e letras.                                                       |
|                                  |         | * ``2`` exige que a senha tenha números, letras e caracteres especiais.                                 |
|                                  |         | * ``3`` exige que a senha tenha números, letras maiúsculas, letras minúsculas e caracteres especiais.   |
+----------------------------------+---------+---------------------------------------------------------------------------------------------------------+
| require_password                 | boolean | Exige que as contas de acesso tenham senha, mesmo sendo contas de serviço.                              |
+----------------------------------+---------+---------------------------------------------------------------------------------------------------------+
| require_email                    | boolean | Exige que as contas de acesso tenham um e-mail cadastrado.                                              |
+----------------------------------+---------+---------------------------------------------------------------------------------------------------------+
| enable_recaptcha                 | boolean | Exige reCaptcha para ações:                                                                             |
|                                  |         |                                                                                                         |
|                                  |         | * Geração de **id_token**.                                                                              |
|                                  |         | * Recuperação de senha.                                                                                 |
+----------------------------------+---------+---------------------------------------------------------------------------------------------------------+
| recaptcha_secret_key             | string  | Define qual a chave secreta do reCaptcha para ser utilizado pelo sistema. Esse campo pode ser ``null``  |
|                                  |         | caso o **enable_recaptcha** seja ``false``.                                                             |
+----------------------------------+---------+---------------------------------------------------------------------------------------------------------+

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"default_password_expiration_days": 60,
		"minimum_password_length": 7,
		"password_security_level": 2,
		"require_password": true,
		"require_email": true,
		"enable_recaptcha": true,
		"recaptcha_secret_key": "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe",
		"short_term_expiration_seconds": 1800,
		"long_term_expiration_seconds": 86400
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"security_configuration_uuid": "c613359b-0d89-419a-95e7-7395c0e68e7e",
		"default_password_expiration_days": 60,
		"minimum_password_length": 7,
		"password_security_level": 2,
		"require_password": true,
		"require_email": true,
		"enable_recaptcha": true,
		"recaptcha_secret_key": "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe",
		"short_term_expiration_seconds": 1800,
		"long_term_expiration_seconds": 86400
	}

-------------------------------------------
uAuthenticator.SecurityConfiguration.update
-------------------------------------------

Altera dados de uma configuração de segurança. Utiliza os mesmos parâmetros do método ``uAuthenticator.SecurityConfiguration.create``.
É obrigatório fornecer o ``security_configuration_uuid`` sendo o UUIDv4 da configuração de segurança que deseja atualizar.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"security_configuration_uuid": "c613359b-0d89-419a-95e7-7395c0e68e7e",
		"default_password_expiration_days": 45,
		"minimum_password_length": 7,
		"password_security_level": 2,
		"require_password": true,
		"require_email": true,
		"enable_recaptcha": false,
		"recaptcha_secret_key": null,
		"short_term_expiration_seconds": 1800,
		"long_term_expiration_seconds": 86400
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"security_configuration_uuid": "c613359b-0d89-419a-95e7-7395c0e68e7e",
		"default_password_expiration_days": 45,
		"minimum_password_length": 7,
		"password_security_level": 2,
		"require_password": true,
		"require_email": true,
		"enable_recaptcha": false,
		"recaptcha_secret_key": null,
		"short_term_expiration_seconds": 1800,
		"long_term_expiration_seconds": 86400
	}

----------------------------------------
uAuthenticator.SecurityConfiguration.get
----------------------------------------

Retorna os dados de uma configuração de segurança.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"security_configuration_uuid": "c613359b-0d89-419a-95e7-7395c0e68e7e"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"security_configuration_uuid": "c613359b-0d89-419a-95e7-7395c0e68e7e",
		"default_password_expiration_days": 45,
		"minimum_password_length": 7,
		"password_security_level": 2,
		"require_password": true,
		"require_email": true,
		"enable_recaptcha": false,
		"recaptcha_secret_key": null,
		"short_term_expiration_seconds": 1800,
		"long_term_expiration_seconds": 86400
	}

-------------------------------------------
uAuthenticator.SecurityConfiguration.search
-------------------------------------------

Busca uma configuração de segurança. Para entender o valor do campo **search_query** visite a documentação da estrutura de busca,
dos microserviços.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"search_query": "page=1&eq[default_password_expiration_days]=45&eq[enable_recaptcha]=false"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"total_count": 1,
		"results": [
			{
				"security_configuration_uuid": "c613359b-0d89-419a-95e7-7395c0e68e7e",
				"default_password_expiration_days": 45,
				"minimum_password_length": 7,
				"password_security_level": 2,
				"require_password": true,
				"require_email": true,
				"enable_recaptcha": false,
				"recaptcha_secret_key": null,
				"short_term_expiration_seconds": 1800,
				"long_term_expiration_seconds": 86400
			}
		]
	}

-------------------------------------------
uAuthenticator.SecurityConfiguration.delete
-------------------------------------------

Deleta uma configuração de segurança. **Atenção!** Essa ação não tem retorno.


^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"security_configuration_uuid": "c613359b-0d89-419a-95e7-7395c0e68e7e"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	"c613359b-0d89-419a-95e7-7395c0e68e7e"