Access
======

Esse serviço fornece de fato o controle de acessos através da autenticação de contas. Ele aplica as políticas definidas pela federação no fluxo de autenticação. Todos os tokens são assinados utilizando o par de chaves definido na federação. Abaixo são descritos os métodos disponíveis do serviço:

----------------------------
uAuthenticator.Access.signIn
----------------------------

Gera um **id_token** de uma conta de acesso.

**Notas:**

* Caso a configuração de segurança da federação não exija reCaptcha você poderá passar uma string vazia no campo **recaptcha_response** (string vazia: ``""``).
* O campo **device_information** pode ser utilizado para identificar o dispositivo onde foi solicitada a recuperação de senha.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"password": "qwerY@12345",
		"identifier": "evosec_security",
		"recaptcha_response": "minha_resposta_recaptcha",
		"device_information": "Chrome versão X/Aplicação Y"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"id_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiaWRfdG9rZW4iLCJpZGVudGlmaWVyIjoiZXZvc2VjX3NlY3VyaXR5IiwiYWNjb3VudF9uYW1lIjoiRXZvc2VjIFNlY3VyaXR5IiwibXVsdGlfZmFjdG9yX21ldGhvZCI6Im5vbmUiLCJpYXQiOjE1MzYzMzM3ODcsImV4cCI6MTUzNjMzNTU4NywiYXVkIjoiZWRkZDgzOTEtOWE3Yy00OTA2LTliZjItOGMwOWEyYzIwM2YwIiwiaXNzIjoiZWRkZDgzOTEtOWE3Yy00OTA2LTliZjItOGMwOWEyYzIwM2YwIiwic3ViIjoiYTU0M2Y4NzYtOTU4ZC00NjM2LTlmYWMtYzgxNzZjNDg5ZTU3IiwianRpIjoiYzdiZTQ1NzgtNzRlYS00ZjMxLWI2NjYtYWZiYzg0NzE3NGM5In0.RmHH0p1RzSo7RxXWkcoJw_LV8ALa7YqXHqIamdDk3PuCpTLLSZpx2fj0vsSaPXtu13EyZIQHWYmQ5acOqOhvLPSnMUfJ7y5BZ_QgvR6qaMEjXLcRDTQVdmb5KGIQQ0ETrM4JUhXbYp2AIEhBebcEMZhtn7oZUmga8oX91CVP2ClEpQ4J885_YhjHQ8WdgWi6uZ3LqwWpHRUT_9_YUwqudPfgESKdK7VyyTdnUOe6tcHrH54tS-pMYgtWa2IYyyUxn2lsCG6JMKJNIFT69xcEKdKZu8b7Es3_9Oq47UkbPRYCA0RZwxQ71yrSeyk-tWYJpYpg2Fr2KxwXzBrdNE0gbQ",
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0"
	}

-----------------------------------------
uAuthenticator.Access.signInGoogleIdToken
-----------------------------------------

Gera um **id_token** de uma conta de acesso utilizando um **id_token** do Google.

**Notas:**

* Esse método funciona apenas para federações possuem ao menos um provedor de identidade, veja a documentação do **IdentityProvider** para entender como habilitar esse serviço.
* Caso não exista nenhuma conta de acesso vinculada ao **id_token** do Google será criada uma conta de acesso sem senha e sem identificadores. Após realizar a autenticação esses campos poderão ser preenchidos.
* Caso a configuração de segurança da federação não exija reCaptcha você poderá passar uma string vazia no campo **recaptcha_response** (string vazia: ``""``).
* O campo **device_information** pode ser utilizado para identificar o dispositivo onde foi solicitada a recuperação de senha.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"id_token": "id_token_Fornecido_Pela_Autenticação_No_Google",
		"recaptcha_response": "minha_resposta_recaptcha",
		"device_information": "Chrome versão X/Aplicação Y"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"id_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiaWRfdG9rZW4iLCJpZGVudGlmaWVyIjoiZXZvc2VjX3NlY3VyaXR5IiwiYWNjb3VudF9uYW1lIjoiRXZvc2VjIFNlY3VyaXR5IiwibXVsdGlfZmFjdG9yX21ldGhvZCI6Im5vbmUiLCJpYXQiOjE1MzYzMzM3ODcsImV4cCI6MTUzNjMzNTU4NywiYXVkIjoiZWRkZDgzOTEtOWE3Yy00OTA2LTliZjItOGMwOWEyYzIwM2YwIiwiaXNzIjoiZWRkZDgzOTEtOWE3Yy00OTA2LTliZjItOGMwOWEyYzIwM2YwIiwic3ViIjoiYTU0M2Y4NzYtOTU4ZC00NjM2LTlmYWMtYzgxNzZjNDg5ZTU3IiwianRpIjoiYzdiZTQ1NzgtNzRlYS00ZjMxLWI2NjYtYWZiYzg0NzE3NGM5In0.RmHH0p1RzSo7RxXWkcoJw_LV8ALa7YqXHqIamdDk3PuCpTLLSZpx2fj0vsSaPXtu13EyZIQHWYmQ5acOqOhvLPSnMUfJ7y5BZ_QgvR6qaMEjXLcRDTQVdmb5KGIQQ0ETrM4JUhXbYp2AIEhBebcEMZhtn7oZUmga8oX91CVP2ClEpQ4J885_YhjHQ8WdgWi6uZ3LqwWpHRUT_9_YUwqudPfgESKdK7VyyTdnUOe6tcHrH54tS-pMYgtWa2IYyyUxn2lsCG6JMKJNIFT69xcEKdKZu8b7Es3_9Oq47UkbPRYCA0RZwxQ71yrSeyk-tWYJpYpg2Fr2KxwXzBrdNE0gbQ",
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0"
	}

------------------------------
uAuthenticator.Access.exchange
------------------------------

Gera um **access_token** e **refresh_token** de um **id_token**. **Atenção!** Esse método só deve ser consumido caso o campo **multi_factor_method** do **id_token** tenha o valor ``"none"``.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"id_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiaWRfdG9rZW4iLCJpZGVudGlmaWVyIjoiZXZvc2VjX3NlY3VyaXR5IiwiYWNjb3VudF9uYW1lIjoiRXZvc2VjIFNlY3VyaXR5IiwibXVsdGlfZmFjdG9yX21ldGhvZCI6Im5vbmUiLCJpYXQiOjE1MzYzMzM3ODcsImV4cCI6MTUzNjMzNTU4NywiYXVkIjoiZWRkZDgzOTEtOWE3Yy00OTA2LTliZjItOGMwOWEyYzIwM2YwIiwiaXNzIjoiZWRkZDgzOTEtOWE3Yy00OTA2LTliZjItOGMwOWEyYzIwM2YwIiwic3ViIjoiYTU0M2Y4NzYtOTU4ZC00NjM2LTlmYWMtYzgxNzZjNDg5ZTU3IiwianRpIjoiYzdiZTQ1NzgtNzRlYS00ZjMxLWI2NjYtYWZiYzg0NzE3NGM5In0.RmHH0p1RzSo7RxXWkcoJw_LV8ALa7YqXHqIamdDk3PuCpTLLSZpx2fj0vsSaPXtu13EyZIQHWYmQ5acOqOhvLPSnMUfJ7y5BZ_QgvR6qaMEjXLcRDTQVdmb5KGIQQ0ETrM4JUhXbYp2AIEhBebcEMZhtn7oZUmga8oX91CVP2ClEpQ4J885_YhjHQ8WdgWi6uZ3LqwWpHRUT_9_YUwqudPfgESKdK7VyyTdnUOe6tcHrH54tS-pMYgtWa2IYyyUxn2lsCG6JMKJNIFT69xcEKdKZu8b7Es3_9Oq47UkbPRYCA0RZwxQ71yrSeyk-tWYJpYpg2Fr2KxwXzBrdNE0gbQ",
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0"
	}

---------------------------------------
uAuthenticator.Access.exchangeTimeToken
---------------------------------------

Gera um **access_token** e **refresh_token** de um **id_token**. **Atenção!** Esse método só deve ser consumido caso o campo **multi_factor_method** do **id_token** tenha o valor ``"time_token"``.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"id_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiaWRfdG9rZW4iLCJpZGVudGlmaWVyIjoiZXZvc2VjX3NlY3VyaXR5IiwiYWNjb3VudF9uYW1lIjoiRXZvc2VjIFNlY3VyaXR5IiwibXVsdGlfZmFjdG9yX21ldGhvZCI6Im5vbmUiLCJpYXQiOjE1MzYzMzM3ODcsImV4cCI6MTUzNjMzNTU4NywiYXVkIjoiZWRkZDgzOTEtOWE3Yy00OTA2LTliZjItOGMwOWEyYzIwM2YwIiwiaXNzIjoiZWRkZDgzOTEtOWE3Yy00OTA2LTliZjItOGMwOWEyYzIwM2YwIiwic3ViIjoiYTU0M2Y4NzYtOTU4ZC00NjM2LTlmYWMtYzgxNzZjNDg5ZTU3IiwianRpIjoiYzdiZTQ1NzgtNzRlYS00ZjMxLWI2NjYtYWZiYzg0NzE3NGM5In0.RmHH0p1RzSo7RxXWkcoJw_LV8ALa7YqXHqIamdDk3PuCpTLLSZpx2fj0vsSaPXtu13EyZIQHWYmQ5acOqOhvLPSnMUfJ7y5BZ_QgvR6qaMEjXLcRDTQVdmb5KGIQQ0ETrM4JUhXbYp2AIEhBebcEMZhtn7oZUmga8oX91CVP2ClEpQ4J885_YhjHQ8WdgWi6uZ3LqwWpHRUT_9_YUwqudPfgESKdK7VyyTdnUOe6tcHrH54tS-pMYgtWa2IYyyUxn2lsCG6JMKJNIFT69xcEKdKZu8b7Es3_9Oq47UkbPRYCA0RZwxQ71yrSeyk-tWYJpYpg2Fr2KxwXzBrdNE0gbQ",
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"time_token":"054333"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzX3Rva2VuIiwic291cmNlX3JlZnJlc2hfdG9rZW5fdXVpZCI6IjVhZDEyOWUwLTM2NDItNDNlYi1iYTFkLTIwMGEwNDdhNDZjYiIsImlhdCI6MTUzNjMzMzk0NywiZXhwIjoxNTM2MzM1NzQ3LCJhdWQiOiJlZGRkODM5MS05YTdjLTQ5MDYtOWJmMi04YzA5YTJjMjAzZjAiLCJpc3MiOiJlZGRkODM5MS05YTdjLTQ5MDYtOWJmMi04YzA5YTJjMjAzZjAiLCJzdWIiOiJhNTQzZjg3Ni05NThkLTQ2MzYtOWZhYy1jODE3NmM0ODllNTciLCJqdGkiOiJmMzAyMTA5ZC0zNWNjLTRkZDUtYmEyNC1jMDY2NWRlYTFlYzAifQ.oWcAQSzHj9YUax8LcMrEXPJQd6h0L-YileEbjgdx9H_Vc3sfkkv_tnQ1NYmgbBofnt_hm6jlcEH62ujPd3Is6ZuwfEmMCobDuHxywgfGFnEi9cqLDHAjm6EtDspcyWTGxVWQP4vR8m9gQsM7A9roVm38DERZI9xhYpJXJaOA6SRtyawQrVrG6rw5w_Qw4I6ks1lkhDB43YNOay2Ke1LgnD6qYnjOikvk4OpuMRDazozSOYRgmb6VXwauxu-U5Xnb8NJ2_aGoXDQ6fT2zdjCEEz7X1YRJePga-s-hfr-R_uiZgmvhxAKJHi15lv5gsL2e5yJmGnVLfDgLP62ZH_YSrg",
		"refresh_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaF90b2tlbiIsImFjY2Vzc190b2tlbl9leHBpcmF0aW9uIjoxODAwLCJpZGVudGlmaWVyIjoiZXZvc2VjX3NlY3VyaXR5IiwiYWNjb3VudF9uYW1lIjoiRXZvc2VjIFNlY3VyaXR5IiwiaWF0IjoxNTM2MzMzOTQ3LCJleHAiOjE1MzY0MjAzNDcsImF1ZCI6ImVkZGQ4MzkxLTlhN2MtNDkwNi05YmYyLThjMDlhMmMyMDNmMCIsImlzcyI6ImVkZGQ4MzkxLTlhN2MtNDkwNi05YmYyLThjMDlhMmMyMDNmMCIsInN1YiI6ImE1NDNmODc2LTk1OGQtNDYzNi05ZmFjLWM4MTc2YzQ4OWU1NyIsImp0aSI6IjVhZDEyOWUwLTM2NDItNDNlYi1iYTFkLTIwMGEwNDdhNDZjYiJ9.m3p3md0LW9HOLbkq0LYHS7ulLdDWuznN07ATu69tD-eeH-b2894RDbcdOUV2dU_aCSqecrsKBlR1YQdUFaerjZ6uxiEEbwvpcbsyaTkVr0brP8avk8yttBHlqcZLwOdqCR24Wlr147SoZWA1NoaWehwjTPb2p3XiGmB_m186yE2-ozggqkRF7O31mC01GlYVh3TKH6iatDVvRTqn86b9ul_u9VbWgpdJxr6yJL3azG6fGC1HrQAKhtvLm8PzFm6c94wcvdjR9pelCBUBsBrBbzH-c979qEvi8sWVyw-55j3bAimxRg4flOV1etthdz7odqR2UK-q5FbOsheq6fCcgQ"
	}

-----------------------------
uAuthenticator.Access.refresh
-----------------------------

Gera um **access_token** com tempo de expiração renovado utilizando um **refresh_token** previamente obtido. **Atenção!** Quando o **refresh_token** expirar será necessário autenticar novamente para gerar um novo **refresh_token**.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"refresh_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaF90b2tlbiIsImFjY2Vzc190b2tlbl9leHBpcmF0aW9uIjoxODAwLCJpZGVudGlmaWVyIjoiZXZvc2VjX3NlY3VyaXR5IiwiYWNjb3VudF9uYW1lIjoiRXZvc2VjIFNlY3VyaXR5IiwiaWF0IjoxNTM2MzMzOTQ3LCJleHAiOjE1MzY0MjAzNDcsImF1ZCI6ImVkZGQ4MzkxLTlhN2MtNDkwNi05YmYyLThjMDlhMmMyMDNmMCIsImlzcyI6ImVkZGQ4MzkxLTlhN2MtNDkwNi05YmYyLThjMDlhMmMyMDNmMCIsInN1YiI6ImE1NDNmODc2LTk1OGQtNDYzNi05ZmFjLWM4MTc2YzQ4OWU1NyIsImp0aSI6IjVhZDEyOWUwLTM2NDItNDNlYi1iYTFkLTIwMGEwNDdhNDZjYiJ9.m3p3md0LW9HOLbkq0LYHS7ulLdDWuznN07ATu69tD-eeH-b2894RDbcdOUV2dU_aCSqecrsKBlR1YQdUFaerjZ6uxiEEbwvpcbsyaTkVr0brP8avk8yttBHlqcZLwOdqCR24Wlr147SoZWA1NoaWehwjTPb2p3XiGmB_m186yE2-ozggqkRF7O31mC01GlYVh3TKH6iatDVvRTqn86b9ul_u9VbWgpdJxr6yJL3azG6fGC1HrQAKhtvLm8PzFm6c94wcvdjR9pelCBUBsBrBbzH-c979qEvi8sWVyw-55j3bAimxRg4flOV1etthdz7odqR2UK-q5FbOsheq6fCcgQ"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzX3Rva2VuIiwic291cmNlX3JlZnJlc2hfdG9rZW5fdXVpZCI6IjVhZDEyOWUwLTM2NDItNDNlYi1iYTFkLTIwMGEwNDdhNDZjYiIsImlhdCI6MTUzNjMzNDAxNywiZXhwIjoxNTM2MzM1ODE3LCJhdWQiOiJlZGRkODM5MS05YTdjLTQ5MDYtOWJmMi04YzA5YTJjMjAzZjAiLCJpc3MiOiJlZGRkODM5MS05YTdjLTQ5MDYtOWJmMi04YzA5YTJjMjAzZjAiLCJzdWIiOiJhNTQzZjg3Ni05NThkLTQ2MzYtOWZhYy1jODE3NmM0ODllNTciLCJqdGkiOiJiOTY4YTRjOS02NTMwLTQ5YTYtOWZiYy1kMWZmMWZhNjBjZDQifQ.X2mYX-ZXyljQP2djtAD_e5mvSiIn6_EUwMUNYZ2J5n_meCORiCsA6p_L125BQEMj5YAHrggWoacftDvJRLWxJJtShmW0lXK-puZhHVq8dSKbqs3d14xmnivoB5V_SO7LmDF0j0LMPAeez0CHb1o044f4eupEcq9knwclleo_xdQCpIbV1jdZqz1dccZmaBwsfOGHyxx3s8chwHHyminqmPPKfcrEWuwSuebKo-5qbwJVI1LlG5MD2KQc1Q_o3a4LijKW8ZeS-gS64OTF8lk4d91jGm_rp_r9RClPiFzKEfgBAOEptze1yCNfWEiTT5YQyFN7_kSTUPrYE7il-erK6Q",
		"refresh_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaF90b2tlbiIsImFjY2Vzc190b2tlbl9leHBpcmF0aW9uIjoxODAwLCJpZGVudGlmaWVyIjoiZXZvc2VjX3NlY3VyaXR5IiwiYWNjb3VudF9uYW1lIjoiRXZvc2VjIFNlY3VyaXR5IiwiaWF0IjoxNTM2MzMzOTQ3LCJleHAiOjE1MzY0MjAzNDcsImF1ZCI6ImVkZGQ4MzkxLTlhN2MtNDkwNi05YmYyLThjMDlhMmMyMDNmMCIsImlzcyI6ImVkZGQ4MzkxLTlhN2MtNDkwNi05YmYyLThjMDlhMmMyMDNmMCIsInN1YiI6ImE1NDNmODc2LTk1OGQtNDYzNi05ZmFjLWM4MTc2YzQ4OWU1NyIsImp0aSI6IjVhZDEyOWUwLTM2NDItNDNlYi1iYTFkLTIwMGEwNDdhNDZjYiJ9.m3p3md0LW9HOLbkq0LYHS7ulLdDWuznN07ATu69tD-eeH-b2894RDbcdOUV2dU_aCSqecrsKBlR1YQdUFaerjZ6uxiEEbwvpcbsyaTkVr0brP8avk8yttBHlqcZLwOdqCR24Wlr147SoZWA1NoaWehwjTPb2p3XiGmB_m186yE2-ozggqkRF7O31mC01GlYVh3TKH6iatDVvRTqn86b9ul_u9VbWgpdJxr6yJL3azG6fGC1HrQAKhtvLm8PzFm6c94wcvdjR9pelCBUBsBrBbzH-c979qEvi8sWVyw-55j3bAimxRg4flOV1etthdz7odqR2UK-q5FbOsheq6fCcgQ"
	}

----------------------------------
uAuthenticator.Access.authenticate
----------------------------------

Autentica uma sessão utilizando um **access_token**, após a chamada desse método a sessão terá todas as permissões da conta de acesso vinculada ao **access_token**.

**Notas:**

* O campo **account_groups_uuids** representa os grupos subordinados dos **associated_groups_uuids** que por sua vez são os grupos de conta que estão vinculados com a conta de acesso.
* O campo **expires_at** representa o número de segundos em formato UNIX timestamp.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzX3Rva2VuIiwic291cmNlX3JlZnJlc2hfdG9rZW5fdXVpZCI6IjVhZDEyOWUwLTM2NDItNDNlYi1iYTFkLTIwMGEwNDdhNDZjYiIsImlhdCI6MTUzNjMzNDAxNywiZXhwIjoxNTM2MzM1ODE3LCJhdWQiOiJlZGRkODM5MS05YTdjLTQ5MDYtOWJmMi04YzA5YTJjMjAzZjAiLCJpc3MiOiJlZGRkODM5MS05YTdjLTQ5MDYtOWJmMi04YzA5YTJjMjAzZjAiLCJzdWIiOiJhNTQzZjg3Ni05NThkLTQ2MzYtOWZhYy1jODE3NmM0ODllNTciLCJqdGkiOiJiOTY4YTRjOS02NTMwLTQ5YTYtOWZiYy1kMWZmMWZhNjBjZDQifQ.X2mYX-ZXyljQP2djtAD_e5mvSiIn6_EUwMUNYZ2J5n_meCORiCsA6p_L125BQEMj5YAHrggWoacftDvJRLWxJJtShmW0lXK-puZhHVq8dSKbqs3d14xmnivoB5V_SO7LmDF0j0LMPAeez0CHb1o044f4eupEcq9knwclleo_xdQCpIbV1jdZqz1dccZmaBwsfOGHyxx3s8chwHHyminqmPPKfcrEWuwSuebKo-5qbwJVI1LlG5MD2KQc1Q_o3a4LijKW8ZeS-gS64OTF8lk4d91jGm_rp_r9RClPiFzKEfgBAOEptze1yCNfWEiTT5YQyFN7_kSTUPrYE7il-erK6Q"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"token_type": "access_token",
		"token_uuid": "b968a4c9-6530-49a6-9fbc-d1ff1fa60cd4",
		"expires_at_timestamp": 1536335817,
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"access_account_uuid": "a543f876-958d-4636-9fac-c8176c489e57",
		"is_master": false,
		"account_groups_uuids": [
			"f243550f-573f-452d-8ce9-1f17c87ed9c4"
		],
		"associated_groups_uuids": [
			"0dded5ef-47b3-42e6-937a-b75fbcd14e7c"
		]
	}

----------------------------
uAuthenticator.Access.finish
----------------------------

Finaliza a autenticação de uma sessão.

**Notas:**

* Caso a sessão não esteja autenticada esse método retorna ``false``.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	"a543f876-958d-4636-9fac-c8176c489e57"

--------------------------------------
uAuthenticator.Access.recoveryPassword
--------------------------------------

Gera token de recuperação de senha de conta de acesso. Esse token será disparado para o usuário através de outro microserviço dedicado para disparo de mensagens.

**Notas:**

* Caso a configuração de segurança da federação não exija reCaptcha você poderá passar uma string vazia no campo **recaptcha_response** (string vazia: ``""``).
* O campo **device_information** pode ser utilizado para identificar o dispositivo onde foi solicitada a recuperação de senha.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"identifier": "evosec_security",
		"recaptcha_response": "resposta_do_recaptcha",
		"device_information": "Chrome versão X/Aplicação Y"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	"evosec_security"

-----------------------------------
uAuthenticator.Access.resetPassword
-----------------------------------

Reseta uma senha utilizando um token de recuperação de senha.

**Notas:**

* O valor retornado é a nova data de expiração da senha. Caso uma string vazia seja retornada a senha não irá expirar.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"password_reset_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoicGFzc3dvcmRfcmVzZXRfdG9rZW4iLCJhY2NvdW50X25hbWUiOiJFdm9zZWMgU2VjdXJpdHkiLCJpYXQiOjE1MzYzNDg3OTgsImV4cCI6MTUzNjQzNTE5OCwiYXVkIjoiZWRkZDgzOTEtOWE3Yy00OTA2LTliZjItOGMwOWEyYzIwM2YwIiwiaXNzIjoiZWRkZDgzOTEtOWE3Yy00OTA2LTliZjItOGMwOWEyYzIwM2YwIiwic3ViIjoiYTU0M2Y4NzYtOTU4ZC00NjM2LTlmYWMtYzgxNzZjNDg5ZTU3IiwianRpIjoiYTRmNzM0MTctOTY4MS00YTg3LWE1Y2ItYjVjZDQ1NzEwMjQyIn0.jFrD2Vx4Lm8OPy5JEC25ympZd1RRXm6D5zUe-fk2yrFdZjbMRLRmM7Cl1JxOBSSEULT2u6b5KGrbsGUa6ua_U91XFee3VPW8KFnHqz1o4PxhSM8tFWSNPYZH78bbEvz3xnwQYrfQQjxghR02yn7SuGFljAnWEq3GmrYv5wptMy-km7P_Gs5ZdUWb2EEvb9knlTn-bIYFb_lhboJiUXunaePocDjUiAUDKuyn-JoEdoQLK_k4uNg2Ik6XWkZSvAZcwXd1wDanoOe9X9DmM4D4iPVWaGnyMUvURRqiZUThMAJS4qcWdhIEsDr1ycMm_Etxth-frOn-lSRLYk_qDgTbFg",
		"password": "novaSenha123#"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	"2018-10-07"