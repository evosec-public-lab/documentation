IdentityProvider
================

Os provedores de identidade são serviços externos que fornecem identidades de contas de acesso ao uAuthenticator como por exemplo Google, Facebook, Twitter e outros.

---------------
AccountIdentity
---------------

Cada conta de acesso pode ter várias identidades ou seja, cadastro em serviços externos que fornecem a identidade da conta. Uma identidade de conta é um cadastro em serviço externo reconhecido como provedor de identidades.

Abaixo são descritos os métodos disponíveis do serviço:

-----------------------------------------------
uAuthenticator.IdentityProvider.addGoogleClient
-----------------------------------------------

Adiciona um cliente do Google como provedor de identidades. Alguns parâmetros são importantes e estão descritos na tabela abaixo:

+-------------------+---------------+------------------------------------------------------------------------------------+
| Campo             | Tipo          | Descrição                                                                          |
+===================+===============+====================================================================================+
| client_id         | string        | O Client ID fornecido pelo Google para requisições OAuth.                          |
+-------------------+---------------+------------------------------------------------------------------------------------+
| name              | string        | Um nome para identificar o provedor de identidades.                                |
+-------------------+---------------+------------------------------------------------------------------------------------+
| federations_uuids | array<UUIDv4> | Um array com os UUIDs das federações as quais o provedor de identidades se aplica. |
+-------------------+---------------+------------------------------------------------------------------------------------+
| label             | string        | Esse campo não precisa ser fornecido, ele será definido pelo uAuthenticator nos    |
|                   |               | provedores de identidade para identificar qual o serviço externo do provedor.      |
+-------------------+---------------+------------------------------------------------------------------------------------+

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"client_id": "221409522768-f2usgp01tg2575rqdqs06q7bi65hh5kq.apps.googleusercontent.com",
		"name":"Google OAuth Client",
		"federations_uuids":[
			"715b7f6c-765d-4357-9637-e53ca6e6e59b"    
		]
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"identity_provider_uuid": "ccbc1056-b724-44e4-ad7d-7bbfda706ad8",
		"client_id": "221409522768-f2usgp01tg2575rqdqs06q7bi65hh5kq.apps.googleusercontent.com",
		"name":"Google OAuth Client",
		"label": "google",
		"federations_uuids":[
			"715b7f6c-765d-4357-9637-e53ca6e6e59b"    
		]
	}

--------------------------------------------------
uAuthenticator.IdentityProvider.updateGoogleClient
--------------------------------------------------

Altera dados de um cliente do Google. Utiliza os mesmos parâmetros do método ``uAuthenticator.IdentityProvider.addGoogleClient``.
É obrigatório fornecer o ``identity_provider_uuid`` sendo o UUIDv4 do provedor de identidade que deseja atualizar.

**Notas:**

* Alterar o campo **client_id** pode invalidar identidades de conta de acesso. É mais recomendado remover as federações para desabilitar um provedor de identidades.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"client_id": "221409522768-f2usgp01tg2575rqdqs06q7bi65hh5kq.apps.googleusercontent.com",
		"name":"Google OAuth Renamed",
		"federations_uuids":[
			"715b7f6c-765d-4357-9637-e53ca6e6e59b"    
		]
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"identity_provider_uuid": "ccbc1056-b724-44e4-ad7d-7bbfda706ad8",
		"client_id": "221409522768-f2usgp01tg2575rqdqs06q7bi65hh5kq.apps.googleusercontent.com",
		"name":"Google OAuth Renamed",
		"label": "google",
		"federations_uuids":[
			"715b7f6c-765d-4357-9637-e53ca6e6e59b"    
		]
	}

-----------------------------------------------
uAuthenticator.IdentityProvider.getGoogleClient
-----------------------------------------------

Retorna os dados de um cliente do Google.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"identity_provider_uuid": "ccbc1056-b724-44e4-ad7d-7bbfda706ad8"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"identity_provider_uuid": "ccbc1056-b724-44e4-ad7d-7bbfda706ad8",
		"client_id": "221409522768-f2usgp01tg2575rqdqs06q7bi65hh5kq.apps.googleusercontent.com",
		"name":"Google OAuth Renamed",
		"label": "google",
		"federations_uuids":[
			"715b7f6c-765d-4357-9637-e53ca6e6e59b"    
		]
	}

--------------------------------------
uAuthenticator.IdentityProvider.delete
--------------------------------------

Deleta um provedor de identidade. **Atenção!** Essa ação não tem retorno.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"identity_provider_uuid": "ccbc1056-b724-44e4-ad7d-7bbfda706ad8"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	"ccbc1056-b724-44e4-ad7d-7bbfda706ad8"

--------------------------------------
uAuthenticator.IdentityProvider.search
--------------------------------------

Busca um cliente do Google. Para entender o valor do campo **search_query** visite a documentação da estrutura de busca,
dos microserviços.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"search_query": "page=1"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"total_count": 1,
		"results": [
			{
				"identity_provider_uuid": "ccbc1056-b724-44e4-ad7d-7bbfda706ad8",
				"label": "google",
				"name": "Google OAuth Client",
				"federations_uuids": [
					"0c385483-639d-4e39-bcd9-0466ff69d56d"
				]
			}
		]
	}

------------------------------------------------
uAuthenticator.IdentityProvider.linkGoogleClient
------------------------------------------------

Adiciona uma identidade fornecida pelo Google como identidade de uma conta de acesso.

**Notas:**

* Esse método só deve ser chamado para sessões já autenticadas, pois fará o link do **id_token** fornecido pelo Google com a conta na sessão atual.
* Será preciso obter um **id_token** válido do Google para realizar o *link* com a conta de acesso da sessão.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"id_token": "id_token_Fornecido_Pela_Autenticação_No_Google"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"account_identity_uuid": "5c66d9a4-97e1-4112-a46b-3ff7b55e1d4c",
		"label": "google",
		"access_account_uuid": "b301a0dc-8984-4e77-842b-752c7842b6aa",
		"federation_uuid": "0c385483-639d-4e39-bcd9-0466ff69d56d",
		"identity_provider_uuid": "ccbc1056-b724-44e4-ad7d-7bbfda706ad8",
		"external_identity_id": "110891256390414251707"
	}

-----------------------------------------------------
uAuthenticator.IdentityProvider.unlinkAccountIdentity
-----------------------------------------------------

Remove a identidade de uma conta de acesso.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"account_identity_uuid": "5c66d9a4-97e1-4112-a46b-3ff7b55e1d4c"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	"5c66d9a4-97e1-4112-a46b-3ff7b55e1d4c"

-----------------------------------------------------
uAuthenticator.IdentityProvider.searchAccountIdentity
-----------------------------------------------------

Busca uma identidade de conta de acesso. Para entender o valor do campo **search_query** visite a documentação da estrutura de busca,
dos microserviços.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"search_query": "page=1"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"total_count": 1,
		"results": [
			{
				"account_identity_uuid": "5c66d9a4-97e1-4112-a46b-3ff7b55e1d4c",
				"label": "google",
				"access_account_uuid": "b301a0dc-8984-4e77-842b-752c7842b6aa",
				"federation_uuid": "0c385483-639d-4e39-bcd9-0466ff69d56d",
				"identity_provider_uuid": "ccbc1056-b724-44e4-ad7d-7bbfda706ad8",
				"external_identity_id": "110891256390414251707"
			}
		]
	}