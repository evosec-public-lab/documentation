AccountGroup
============

Os grupos de contas agrupam contas de acesso. No uAuthenticator apenas são gerenciados como níveis de hierarquia, mas não geram nenhum efeito adicional. São úteis para gerenciar políticas de **autorização**.

Em uma hierarquia os grupos de acesso podem ser classificados em:

* **Subordinados** - Grupos que estão abaixo de outros em nível de hierarquia.
* **Superordenados** - Grupos que estão acima de outros em nível de hierarquia.
* **Associados** - Grupos aos quais um usuário está associado diretamente.

Abaixo são descritos os métodos disponíveis do serviço:

----------------------------------
uAuthenticator.AccountGroup.create
----------------------------------

Cria um grupo de acesso. Alguns parâmetros são importantes e estão descritos na tabela abaixo:

+------------+---------------+--------------------------------------------------------------------------------------+
| Campo      | Tipo          | Descrição                                                                            |
+============+===============+======================================================================================+
| name       | i18n          | Objeto com as traduções do nome do grupo.                                            |
+------------+---------------+--------------------------------------------------------------------------------------+
| attributes | array<object> | Um array de objetos contendo valores customizados de atributos do grupo de contas.   |
|            |               | Esse campo pode ser utilizado para definir parâmetros customizados para front-end ou |
|            |               | outros sistemas/aplicações. Esse campo pode ser um array vazio.                      |
+------------+---------------+--------------------------------------------------------------------------------------+

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"name": {
				"por": "Gerente",
				"eng": "Manager"
		},
		"attributes": [
				{
						"name": "meu_atributo",
						"value": "meu_valor"
				}
			]
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c",
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"name": {
			"eng": "Manager",
			"por": "Gerente"
		},
		"attributes": [
			{
				"name": "meu_atributo",
				"value": "meu_valor"
			}
		]
	}

----------------------------------
uAuthenticator.AccountGroup.update
----------------------------------

Altera dados de um grupo de contas. Utiliza os mesmos parâmetros do método ``uAuthenticator.AccountGroup.create``.
É obrigatório fornecer o ``account_group_uuid`` sendo o UUIDv4 do grupo de contas que deseja atualizar.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c",
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"name": {
			"eng": "IT Manager",
			"por": "Gerente de TI"
		},
		"attributes": [
			{
				"name": "meu_atributo",
				"value": "meu_valor"
			}
		]
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c",
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"name": {
			"eng": "IT Manager",
			"por": "Gerente de TI"
		},
		"attributes": [
			{
				"name": "meu_atributo",
				"value": "meu_valor"
			}
		]
	}

-------------------------------
uAuthenticator.AccountGroup.get
-------------------------------

Retorna os dados de um grupo de contas.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c",
		"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
		"name": {
			"eng": "IT Manager",
			"por": "Gerente de TI"
		},
		"attributes": [
			{
				"name": "meu_atributo",
				"value": "meu_valor"
			}
		]
	}

----------------------------------
uAuthenticator.AccountGroup.search
----------------------------------

Busca um grupo de contas. Para entender o valor do campo **search_query** visite a documentação da estrutura de busca,
dos microserviços.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"search_query": "page=1&sch[i18n.name]=Gerente&language=por"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"total_count": 1,
		"results": [
			{
				"account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c",
				"federation_uuid": "eddd8391-9a7c-4906-9bf2-8c09a2c203f0",
				"name": {
					"por": "Gerente de TI"
				},
				"attributes": [
					{
						"name": "meu_atributo",
						"value": "meu_valor"
					}
				]
			}
		]
	}

----------------------------------
uAuthenticator.AccountGroup.delete
----------------------------------

Deleta um grupo de contas. **Atenção!** Essa ação não tem retorno.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	"0dded5ef-47b3-42e6-937a-b75fbcd14e7c"

------------------------------------------
uAuthenticator.AccountGroup.addSubordinate
------------------------------------------

Adiciona um grupo como subordinado de outro.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"superordinate_account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c",
		"subordinate_account_group_uuid": "f243550f-573f-452d-8ce9-1f17c87ed9c4"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"superordinate_account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c",
		"subordinate_account_group_uuid": "f243550f-573f-452d-8ce9-1f17c87ed9c4"
	}

---------------------------------------------
uAuthenticator.AccountGroup.removeSubordinate
---------------------------------------------

Remove um grupo subordinado de outro.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"superordinate_account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c",
		"subordinate_account_group_uuid": "f243550f-573f-452d-8ce9-1f17c87ed9c4"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"superordinate_account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c",
		"subordinate_account_group_uuid": "f243550f-573f-452d-8ce9-1f17c87ed9c4"
	}

----------------------------------------
uAuthenticator.AccountGroup.getHierarchy
----------------------------------------

Retorna a hierarquia de um grupo de contas.

**Notas:**

* Nesse caso o campo **associated** sempre será ``null``.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"superordinates": [
			{
				"account_group_uuid": "5e0ad612-8f57-4930-832f-00f4c819920b"
			}
		],
		"subordinates": [
			{
				"account_group_uuid": "f243550f-573f-452d-8ce9-1f17c87ed9c4"
			}
		],
		"associated": null
	}

-------------------------------------
uAuthenticator.AccountGroup.addMember
-------------------------------------

Adiciona uma conta de acesso como membro de um grupo de acesso.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c",
		"access_account_uuid": "a543f876-958d-4636-9fac-c8176c489e57"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c",
		"access_account_uuid": "a543f876-958d-4636-9fac-c8176c489e57"
	}

----------------------------------------
uAuthenticator.AccountGroup.removeMember
----------------------------------------

Remove uma conta de acesso de um grupo de acesso.

^^^^^^^^^^
Requisição
^^^^^^^^^^
.. code-block:: json

	{
		"account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c",
		"access_account_uuid": "a543f876-958d-4636-9fac-c8176c489e57"
	}

^^^^^^^^
Resposta
^^^^^^^^
.. code-block:: json

	{
		"account_group_uuid": "0dded5ef-47b3-42e6-937a-b75fbcd14e7c",
		"access_account_uuid": "a543f876-958d-4636-9fac-c8176c489e57"
	}