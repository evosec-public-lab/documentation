Estrutura dos Serviços
======================

O uAuthenticator se divide em serviços dedicados ao controle de autenticação de usuários. Para entendermos o microserviço vamos definir cada serviço contido no mesmo:

=================
FederationManager
=================

O FederationManager é o serviço responsável por gerenciar as federações. Federações são divisões lógicas do fluxo de autenticação, permitindo que o microserviço controle várias políticas diferentes para usuários e grupos em cada federação.

Cada federação possui dois parâmetros de segurança: a configuração e o comportamento. Duas federações podem compartilhar as mesmas entidades de segurança.

=======================
SecurityBehaviorManager
=======================

O SecurityBehaviorManager controla os comportamentos de segurança permitidos no sistema. Por comportamento de segurança entende-se: qualquer parâmetro de segurança relacionado ao acesso do usuário que possa ser definido por usuário.

============================
SecurityConfigurationManager
============================

O SecurityConfigurationManager controla as configurações de segurança permitidos no sistema. Por configuração de segurança entende-se: parâmetros que controlam e definem os requisitos de segurança por federação.

====================
AccessAccountManager
====================

O AccessAccountManager opera as contas de acesso do sistema (tanto contas de usuário quanto contas de serviço). As contas de acesso são as menores unidades de autenticação do microserviço. Elas podem representar uma pessoa ou um sistema que consuma a API.

==================
AccessGroupManager
==================

O AccessGroupManager gerencia os grupos de usuário no sistema. Os grupos de usuário não geram grandes impactos no uAuthenticator, são úteis para outros microserviços que tenham políticas de ACL. Grupos de usuário podem ser organizados em hierarquias e classificados em:

* **Subordinados** - Grupos que estão abaixo de outros em nível de hierarquia.
* **Superordenados** - Grupos que estão acima de outros em nível de hierarquia.
* **Associados** - Grupos aos quais um usuário está associado diretamente.

=============
AccessManager
=============

O AccessManager é um serviço que utiliza as outras entidades para gerenciar os acessos. Também trabalha com recuperação de senha, validação de reCaptcha entre outros. É de fato onde ocorre a autenticação das contas de acesso.

=======================
IdentityProviderManager
=======================

O IdentityProviderManager é um serviço responsável por gerenciar fornecedores de identidade externos como Google, Facebook, Twitter e outros.
